/*

Voici le tableau de correspondances entre notes MIDI / entrées 4051 ( lettres et pins) / sorties 4051 (numéros et pins)

  notes   4051 IN   4051 IN    4051 OUT  4051 OUT
          Lettres     pin      numéros      pin
    60  |    A    |    13    |    1    |    13
    61  |    A    |    13    |    2    |    14
    62  |    A    |    13    |    3    |    15
    63  |    A    |    13    |    4    |    12
    64  |    A    |    13    |    5    |    1
    65  |    A    |    13    |    6    |    5
    66  |    A    |    13    |    7    |    2
    67  |    A    |    13    |    8    |    4
    68  |    B    |    14    |    1    |    13
    69  |    B    |    14    |    2    |    14
    70  |    B    |    14    |    3    |    15
    71  |    B    |    14    |    4    |    12
    72  |    B    |    14    |    5    |    1
    73  |    B    |    14    |    6    |    5
    74  |    B    |    14    |    7    |    2
    75  |    B    |    14    |    8    |    4
    76  |    C    |    15    |    1    |    13
    77  |    C    |    15    |    2    |    14
    78  |    C    |    15    |    3    |    15
    79  |    C    |    15    |    4    |    12
    80  |    C    |    15    |    5    |    1
    81  |    C    |    15    |    6    |    5
    82  |    C    |    15    |    7    |    2
    83  |    C    |    15    |    8    |    4





*/
