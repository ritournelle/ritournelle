/*
Issu du CMOS cookbook, à propose du 4051 multiplexer :
 Si le pin 6 (INH) est HIGH, aucun canal n'est sélectionné. 
 Si le pin 6 est LOW, le canal selectionné est déterminé par les bits envoyés à A, B ou C (entrées de sélection aux pins 9, 10 et 11)
 */

int INH      = 2;   // vers INH des 2 4051
int selIN_A  = 3;   // vers SEL A du 4051 en multiplexe (entrée)
int selIN_B  = 4;   // vers SEL B du 4051 en multiplexe (entrée)
int selIN_C  = 5;   // vers SEL C du 4051 en multiplexe (entrée)
int selOUT_A = 6;   // vers SEL A du 4051 en demultiplexe (sortie)
int selOUT_B = 7;   // vers SEL B du 4051 en demultiplexe (sortie)
int selOUT_C = 8;   // vers SEL C du 4051 en demultiplexe (sortie)
int pot_delay= A0;  // potentiometre de selection du delais entre chaque déclenchement de note
int pot_value= 0;   // valeur du potentiometre
int ledPin   = 9;   // témoin pour la vitesse de délai

byte incomingByte;  // les données que l'on reçoit depuis le port série de l'entrée MIDI
byte note;          // la valeur de la note
byte velocity;      // sa vélocité
int action=2; //0 =note off ; 1=note on ; 2= rien

/*
********************************************************************************
 ********************************************************************************
 ********************************************************************************
 */

void setup() {
  // lecture du port MIDI par l'entrée RX  
  Serial.begin(31250);
  // pins de sortie pour selectionner les entrées du 4051 en multiplexe   
  pinMode(selIN_A,OUTPUT);
  pinMode(selIN_C,OUTPUT);
  pinMode(selIN_B,OUTPUT);
  // pins de sortie pour selectionner les sorties du 4051 en demultiplexe
  pinMode(selOUT_A,OUTPUT);
  pinMode(selOUT_B,OUTPUT);
  pinMode(selOUT_C,OUTPUT);
  // pin commun pour gérer le INH des 2 4051
  pinMode(INH, OUTPUT);

  pinMode(ledPin, OUTPUT);  
}

void loop() {
  if(Serial.available() > 0){
    incomingByte = Serial.read();
    Serial.println(incomingByte);

    // le message '144' correspond à une note ON
    if (incomingByte== 144){ 
      action=1;
    }
    // le message '128' correspond à une note OFF
    else if (incomingByte== 128){ 
      action=0;
    }
    else if (incomingByte== 208){ // aftertouch message starting
      //not implemented yet
    }
    else if (incomingByte== 160){ // polypressure message starting
      //not implemented yet
    }
    // Si on reçoit une note OFF, on attend pour savoir de quelle note il s'agit (incomingByte)
    else if ( (action==0)&&(note==0) ){
      note=incomingByte;
      playNote(note, 0);
      note=0;
      velocity=0;
      action=2;
    }
    // Si on reçoit une note ON, on attend pour savoir de quelle note il s'agit (incomingByte)
    else if ( (action==1)&&(note==0) ){ 
      note=incomingByte;
    }
    // ...et la vélocité 
    else if ( (action==1)&&(note!=0) ){ 
      velocity=incomingByte;
      playNote(note, velocity);
      note=0;
      velocity=0;
      action=0;
    }
    else{
      //rien de rien
    }
  } 
  // si on ne reçoit pas de signal série alors on il faut "relever" le inhibit pour éviter que le son perdure.
  else{
    //toujours rien de rien
    digitalWrite(INH, HIGH);
    delay(70);
  }
}



