void playNote(byte note, byte velocity){

  // permet de choisir l'entrée du 4051 qui sert de multiplexer. 
  // Ainsi on obtient ici une valeur entre 0 et 7 qui correspond à une valeur entre A et H.
  // On peut retrouver dans l'onglet tableau de correspondance la lettre qui convient à la note
  int   choixEntree = (note-60) / 8;
  // On choisi ici l'entrée du 4051 qui sert de demultiplexer.
  // Ainsi on obitent une valeur entre 0 et 7 qui correspond à la valeur à associer avec la lettre.
  // Par ex la note 113 correspond à la combinaison G6, elle déclenchera ainsi le bouton connecté à cet endroit.
  float choixSortie = (((note-60.0)  / 8.0) - choixEntree) * 8.0;
  // Ici on récupère la valeur d'un potar pour définir le délai entre chaque déclenchement de note.
  pot_value = analogRead(pot_delay);
  float valeur_de_delai = map(pot_value, 0, 1023, 20, 70);

  digitalWrite(INH, LOW);
  digitalWrite(ledPin, HIGH);
  // sélection du point de connexion en entrée
  digitalWrite(selIN_A, SELECTION[choixEntree][0]);
  digitalWrite(selIN_B, SELECTION[choixEntree][1]);
  digitalWrite(selIN_C, SELECTION[choixEntree][2]);
  // sélection du point de connexion en sortie
  digitalWrite(selOUT_A, SELECTION[(int)choixSortie][0]);
  digitalWrite(selOUT_B, SELECTION[(int)choixSortie][1]);
  digitalWrite(selOUT_C, SELECTION[(int)choixSortie][2]);
  // à changer parfois sur certains jouets (allonger le delay)
  // est géré avec un potentiometre
  delay(valeur_de_delai);
  digitalWrite(INH, HIGH);
  digitalWrite(ledPin, LOW);

}














