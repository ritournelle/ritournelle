/*
Issu du CMOS cookbook, à propose du 4051 multiplexer :
 Si le pin 6 (INH) est HIGH, aucun canal n'est sélectionné. 
 Si le pin 6 est LOW, le canal selectionné est déterminé par les bits envoyés à A, B ou C (entrées de sélection aux pins 9, 10 et 11)
 */

#include <MIDI.h>
MIDI_CREATE_DEFAULT_INSTANCE();

int INH      = 5;   // vers INH des 2 4051
int selIN_A  = 2;   // vers SEL A du 4051 en multiplexe (entrée)
int selIN_B  = 3;   // vers SEL B du 4051 en multiplexe (entrée)
int selIN_C  = 4;   // vers SEL C du 4051 en multiplexe (entrée)
int selOUT_A = 6;   // vers SEL A du 4051 en demultiplexe (sortie)
int selOUT_B = 7;   // vers SEL B du 4051 en demultiplexe (sortie)
int selOUT_C = 8;   // vers SEL C du 4051 en demultiplexe (sortie)
int pot_delay= A0;  // potentiometre de selection du delais entre chaque déclenchement de note
int pot_value= 0;   // valeur du potentiometre
int ledPin   = 9;   // témoin pour la vitesse de délai

byte incomingByte;  // les données que l'on reçoit depuis le port série de l'entrée MIDI
byte note;          // la valeur de la note
byte velocity;      // sa vélocité
int action=2; //0 =note off ; 1=note on ; 2= rien

// tableau contenant toutes les configuration de selection possibles
// des entrées A, B et C, pour les 4051.
boolean SELECTION [8][3] = {
  {
    false, false, false
  }
  , {
    true, false, false
  }
  , {
    false, true, false
  }
  , {
    true, true, false
  }
  , {
    false, false, true
  }
  , {
    true, false, true
  }
  , {
    false, true, true
  }
  , {
    true, true, true
  }
};

/*
********************************************************************************
 ********************************************************************************
 ********************************************************************************
 */

void setup() {
  // lecture du port MIDI par l'entrée RX  
  Serial.begin(31250);
  MIDI.begin();           // Launch MIDI, by default listening to channel 1.
  // pins de sortie pour selectionner les entrées du 4051 en multiplexe   
  pinMode(selIN_A,OUTPUT);
  pinMode(selIN_C,OUTPUT);
  pinMode(selIN_B,OUTPUT);
  // pins de sortie pour selectionner les sorties du 4051 en demultiplexe
  pinMode(selOUT_A,OUTPUT);
  pinMode(selOUT_B,OUTPUT);
  pinMode(selOUT_C,OUTPUT);
  // pin commun pour gérer le INH des 2 4051
  pinMode(INH, OUTPUT);
  pinMode(ledPin, OUTPUT);  
}

void loop() {
  if (MIDI.read())                // Is there a MIDI message incoming ?
  {
    switch(MIDI.getType())      // Get the type of the message we caught
    {
      case midi::NoteOn:       // 
      note = MIDI.getData1();  // 
      velocity = MIDI.getData2();
      playNote(note, velocity);
      break;
    default:
      break;
    }
  }
}










